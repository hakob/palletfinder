import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    NativeAppEventEmitter,
    NativeEventEmitter,
    NativeModules,
    Platform,
    PermissionsAndroid,
    ListView,
    ScrollView,
    AppState,
    Dimensions,
    BackHandler,
    TouchableOpacity,
    ART,
    Alert,
    PixelRatio,
} from 'react-native';
import BleManager from 'react-native-ble-manager';
import {stringToBytes} from 'convert-string';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, List, ListItem, Divider} from 'react-native-elements';
import Modal from "react-native-modal";
import * as Progress from 'react-native-progress';
import TabNavigator from 'react-native-tab-navigator';
import sendFile from '../dfu/file-transfer';
import psendFile from '../dfu/profile-tramsfer';
import profilej from '../components/profile';
import Spinner from 'react-native-loading-spinner-overlay';
import {NordicDFU, DFUEmitter} from "react-native-nordic-dfu";
import {Uint64BE} from "int64-buffer";
import {Buffer} from 'buffer';
import RNFS from 'react-native-fs';
import RNGRP from 'react-native-get-real-path';


const FilePicker = NativeModules.FileChooser
const deviceW = Dimensions.get('window').width

const basePx = 375;


function px2dp(px) {
    return px * deviceW / basePx
}

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
let typeMapper = {
    9: 'kCBAdvDataLocalName',
    7: 'kCBAdvDataServiceUUIDs'
};

export default class Scan extends Component {
    constructor() {
        super();
        this.state = {
            imagefile: "",
            uprogress: 0,
            progress: 0,
            modalVisible: false,
            scanning: false,
            peripherals: new Map(),
            appState: '',
            selectedTab: 'home',
            selectedItem: {},
            light: '0',
            load: '0',
            temperature: '0',
            spinner: false,
            dfuState: "Not started",
            file: undefined,
            nameFile: '',
            espName: '',
            indeterminate: true
        };
        this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
        this.handleStopScan = this.handleStopScan.bind(this);
        this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
        this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
        this.handleAppStateChange = this.handleAppStateChange.bind(this);
        this.startDFU = this.startDFU.bind(this);
        this.handleReboot = this.handleReboot.bind(this);
        this.handledfu = this.handledfu.bind(this);
        this.handleDormand = this.handleDormand.bind(this);
    }

    selectFileTapped() {
        const options = {
            title: 'File Chooser',
            chooseFileButtonTitle: 'Choose File...',
        };

        FilePicker.show(options, response => {
            console.log('Response = ', response)

            if (response.didCancel) {
                console.log('User cancelled photo chooser')
            } else if (response.error) {
                console.log('ImageChooserManager Error: ', response.error)
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton)
            } else {
                let name = response.fileName;

                RNGRP.getRealPathFromURI(response.uri).then(path =>
                    this.setState({
                        nameFile: name,
                        file: path
                    })
                );

                console.log(this.state.file)
            }
        })
    }

    button() {
        Alert.alert(
            'Sucsess',
            'Transfer completed',
            [
                {text: 'OK', onPress: () => this.setState({progress: 0}), style: 'cancel'},

            ]
        );
    }

    error() {
        Alert.alert(
            'Transfer failed',
            'Please try again',
            [
                {text: 'OK', onPress: () => this.setState({progress: 0}), style: 'cancel'},

            ]
        );
    }

    selectFile() {
        const options = {
            title: 'File Chooser',
            chooseFileButtonTitle: 'Choose File...',
        };

        FilePicker.show(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo chooser')
            } else if (response.error) {
                console.log('ImageChooserManager Error: ', response.error)
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton)
            } else {
                let name = response.fileName;
                RNGRP.getRealPathFromURI(response.uri).then(path =>
                    RNFS.readFile(path, 'base64').then(imageBase64 => {
                            this.setState({
                                espName: name,
                                imagefile: imageBase64


                            })
                        }
                    ))


                console.log(this.state.file)
            }
        })
    }


    toggleModal(visible) {
        this.setState({modalVisible: visible});
    }

    closeModal() {
        BleManager.disconnect(this.state.selectedItem.itemID)
        this.setState({
            modalVisible: false,
            imagefile: "",
            progress: 0,
            uprogress: 0,
            file: undefined,
            nameFile: "",
            espName: "",
            dfuState: 'Not started'
        });
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backPress)
        AppState.addEventListener('change', this.handleAppStateChange);
        BleManager.start({showAlert: true, forceLegacy: true}).then(() => {
            if (Platform.OS === 'android') {
                BleManager.enableBluetooth().then(() => {
                    // Success code
                    console.log('The bluetooth is already enabled or the user confirm');
                }).catch((error) => {
                    // Failure code
                    console.log('The user refuse to enable bluetooth');
                });
            }
        });

        this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
        this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
        this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral);
        this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic);
        if (Platform.OS === 'android' && Platform.Version >= 23) {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                } else {
                    PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                        if (result) {
                            console.log("User accept");
                        } else {
                            console.log("User refuse");
                        }
                    });
                }
            });
        }

        DFUEmitter.addListener("DFUProgress", ({percent}) => {
            console.log("DFU progress:", percent);
            this.setState({uprogress: percent});
        });
        DFUEmitter.addListener("DFUStateChanged", ({state}) => {
            console.log("DFU state:", state);
            if (state === "DFU_FAILED") {

                this.setState({dfuState: "WAIT..."})
            }
            else {
                this.setState({dfuState: state});
            }
        });
    }

    handleAppStateChange(nextAppState) {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App has come to the foreground!')
            BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
                console.log('Connected peripherals: ' + peripheralsArray.length);
            });
        }
        this.setState({appState: nextAppState});
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backPress)
        this.handlerDiscover.remove();
        this.handlerStop.remove();
        this.handlerDisconnect.remove();
        this.handlerUpdate.remove();
        this.handlerDeviceDiscover.remove()

    }

    handleDisconnectedPeripheral(data) {
        let peripherals = this.state.peripherals;
        let peripheral = peripherals.get(data.peripheral);
        if (peripheral) {
            peripheral.connected = false;
            peripherals.set(peripheral.id, peripheral);
            this.setState({peripherals});
        }
        console.log('Disconnected from ' + data.peripheral);
    }

    handleUpdateValueForCharacteristic(data) {
        console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
    }

    handleStopScan() {
        console.log('Scan is stopped');
        this.setState({scanning: false});
    }

    startScan() {
        if (!this.state.scanning) {
            this.setState({peripherals: new Map()});
            BleManager.scan([], 10, true).then((results) => {
                console.log('Scanning...');
                this.setState({scanning: true});
            });
        }
    }

    startDFU(retry = 5) {

        NordicDFU.startDFU({
            deviceAddress: this.state.selectedItem.itemID,
            name: "BXB",
            filePath: this.state.file
        }).then(res => console.log("Transfer done: ", res)).catch(err => {
            console.log('Error ON DFU', retry);
            if (retry > 0) {
                setTimeout(() => {
                    this.startDFU(retry - 1);
                }, 2000)
            }
        })
    }

    handleDiscoverPeripheral(peripheral) {
        const peripherals = this.state.peripherals;
        if (!peripherals.has(peripheral.id)) {
            console.log('Got ble peripheral', peripheral);
            peripherals.set(peripheral.id, peripheral);
            this.setState({peripherals})
        }
    }

    handleReboot(id) {
        const service = '10000023-1212-EFDE-1523-785FEABCD123';
        const lightCharacteristic = '6E402222-B5A3-F393-E0A9-E50E24DCCA9E';
        const data = stringToBytes('0101');
        BleManager.write(id, service, lightCharacteristic, data)
    };

    handleDormand(id) {
        const service = '10000023-1212-EFDE-1523-785FEABCD123';
        const lightCharacteristic = '6E402222-B5A3-F393-E0A9-E50E24DCCA9E';
        const data = stringToBytes('0103');
        BleManager.write(id, service, lightCharacteristic, data)
    };

    handledfu(id) {
        const service = '10000023-1212-EFDE-1523-785FEABCD123';
        const lightCharacteristic = '6E402222-B5A3-F393-E0A9-E50E24DCCA9E';
        const data = stringToBytes('0102');
        BleManager.write(id, service, lightCharacteristic, data)
    };

    backPress = () => {
        BackHandler.exitApp();
    };

    test(peripheral) {
        if (peripheral) {
            if (peripheral.connected) {
                BleManager.disconnect(peripheral.id);
                this.setState({light: '0', load: '0'})
            } else {
                BleManager.connect(peripheral.id).then(() => {
                    let peripherals = this.state.peripherals;
                    let p = peripherals.get(peripheral.id);
                    if (p) {
                        p.connected = true;
                        peripherals.set(peripheral.id, p);
                        this.setState({peripherals, spinner: false});
                    }

                    setTimeout(() => {
                        BleManager.retrieveServices(peripheral.id).then((peripheralInfo) => {
                            const service = '00001523-1212-EFDE-1523-785FEABCD123';
                            const lightCharacteristic = '00001525-1212-EFDE-1523-785FEABCD123';
                            const data = stringToBytes('1');
                            BleManager.write(peripheral.id, service, lightCharacteristic, data).then(() => {

                            });

                            setTimeout(() => {
                                BleManager.read(peripheral.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6e400003-b5a3-f393-e0a9-e50e24dcca9e")
                                    .then((readData) => {
                                        let arr = readData;
                                        let celARR = arr.slice(8, 10)
                                        let sensorARR = arr.slice(0, 8);
                                        const a = new Uint8Array(sensorARR);
                                        const d = new DataView(a.buffer);
                                        const c = new Uint8Array(celARR);
                                        const f = new DataView(c.buffer);
                                        const lights = d.getUint32(0);
                                        const loads = d.getUint32(4);
                                        const temper = f.getInt16(0).toString().replace(/(\d{2})/, "$1.");

                                        this.setState({light: lights, load: loads, temperature: temper})


                                    })
                                    .catch((error) => {

                                    });
                            }, 100)
                        });
                    }, 100);

                    setTimeout(() => {
                        this.toggleModal()
                    }, 100)
                }).catch((error) => {
                    console.log('Connection error', error);
                });
            }
        }
    }

    render() {
        const list1 = Array.from(this.state.peripherals.values());
        let list;
        if (Platform.OS === 'android') {
            list = list1.filter(function (val) {
                return val.name === 'BXB'
            })
        } else {
            list = list1.filter(function (val) {
                return val.advertising.kCBAdvDataLocalName === 'BXB'
            })
        }
        const dataSource = ds.cloneWithRows(list);
        if (this.state.progress > 1) {
            this.button()
        }
        return (

            <View style={styles.container}>
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Connecting...'}
                    textStyle={{color:"black"}}
                />
                <View style={{
                    flexDirection: 'row',
                    padding: 20,
                    backgroundColor: '#f0f0f0',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Icon name="bluetooth" size={50} color="#4F8EF7"
                          style={{alignItems: 'center', justifyContent: 'center'}}/></View>
                <TouchableHighlight style={{
                    marginTop: 25, margin: 20, padding: 20, borderWidth: 2,
                    borderRadius: 10, backgroundColor: this.state.scanning ? '#4F8EF7' : '#ccc'
                }} onPress={() => this.startScan()}>
                    <Text style={{fontSize: 20, fontWeight: 'bold', textAlign: 'center'}}> Scan Devices
                        ({this.state.scanning ? 'on' : 'off'})</Text>
                </TouchableHighlight>
                <Modal animationType={"slide"} transparent={false}
                       visible={this.state.modalVisible}
                       style={{
                           backgroundColor: 'white', flex: 1, borderRadius: 0, borderWidth: 2,
                           borderColor: '#cccccc'
                       }}>{this.state.selectedItem.name === 'BXB' ?
                    <TabNavigator style={styles.nav}>
                        <TabNavigator.Item
                            style={{flex: 1}}
                            selected={this.state.selectedTab === 'home'}
                            title="Status"
                            selectedTitleStyle={{color: "#3496f0"}}
                            renderIcon={() => <Icon name="bars" size={px2dp(22)} color="#666"/>}
                            renderSelectedIcon={() => <Icon name="bars" size={px2dp(22)}
                                                            color="#3496f0"/>}
                            onPress={() => this.setState({
                                selectedTab: 'home'
                            })}>
                            <View style={styles.container2}>
                                <Text style={{
                                    fontSize: 25,
                                    padding: 15,
                                    fontWeight: 'bold',
                                    textAlign: 'center',
                                }}> Sensor info </Text>
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        progress: 0
                                    });
                                    this.closeModal();

                                }} style={{
                                    backgroundColor: "#cccccc",
                                    position: 'absolute',
                                    alignItems: 'center',
                                    top: 0,
                                    left: 0,
                                    width: 30,
                                    height: 30
                                }}>
                                    <Icon size={30} borderRadius={0}
                                          iconStyle={{marginRight: 0}}
                                          name="times" color="#ffffff"
                                          backgroundColor="#00000"/>
                                </TouchableOpacity>
                                <List containerStyle={{
                                    flex: 1,
                                    backgroundColor: '#F5FCFF',
                                    marginTop: 0
                                }}>

                                    <ListItem
                                        leftIcon={{name: "dashboard"}}
                                        title="CHEP SN"
                                        rightTitle={`${this.state.selectedItem.chep}`}
                                        hideChevron
                                        style={{
                                            height: 30
                                        }}
                                    />

                                    <ListItem
                                        leftIcon={{name: "dashboard"}}
                                        title="Version"
                                        rightTitle={`${this.state.selectedItem.ver1}.${this.state.selectedItem.ver2}.${this.state.selectedItem.ver1}`}
                                        hideChevron
                                        style={{
                                            height: 30
                                        }}
                                    />

                                    <ListItem
                                        leftIcon={{name: "dashboard"}}
                                        title="Device Type"
                                        rightTitle={`${this.state.selectedItem.type}`}
                                        hideChevron
                                        style={{
                                            height: 30
                                        }}
                                    />
                                    <ListItem
                                        leftIcon={{name: "dashboard"}}
                                        title="IMEI"
                                        rightTitle={`${this.state.selectedItem.imei}`}
                                        hideChevron
                                        style={{
                                            height: 30
                                        }}
                                    />

                                    <TouchableHighlight
                                        style={{
                                            alignItems: 'center',
                                            backgroundColor: '#f73b1a',
                                            padding: 10,
                                            marginTop: 40,

                                        }}
                                        onPress={() => {
                                            this.handleReboot(this.state.selectedItem.itemID)
                                        }}>
                                        <Text style={{color: "white"}}>Reboot device</Text>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        style={{
                                            alignItems: 'center',
                                            backgroundColor: '#f7d441',
                                            padding: 10,
                                            marginTop: 40,

                                        }}
                                        onPress={() => {
                                            this.handleDormand(this.state.selectedItem.itemID)
                                        }}>
                                        <Text style={{color: "white"}}>Dormant mode</Text>
                                    </TouchableHighlight>
                                </List>
                            </View>
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'profile'}
                            title="Config"
                            selectedTitleStyle={{color: "#3496f0"}}
                            renderIcon={() => <Icon name="wrench" size={px2dp(22)} color="#666"/>}
                            renderSelectedIcon={() => <Icon name="wrench" size={px2dp(22)}
                                                            color="#3496f0"/>}
                            onPress={() => {
                                this.setState({selectedTab: 'profile'})
                            }}>
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                backgroundColor: '#F5FCFF',

                            }}>
                                <Text style={{
                                    fontSize: 25,
                                    padding: 15,
                                    fontWeight: 'bold',
                                    textAlign: 'center',
                                }}> File Transfer </Text>
                                <Text style={{
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    textAlign: 'center',
                                    marginBottom: 20
                                }}> ESP Image Transfer </Text>

                                <Icon name="download" size={50} color={'#3496f0'}/>
                                <TouchableOpacity

                                    onPress={this.selectFile.bind(this)}
                                    underlayColor="0A64C2" style={{
                                    borderColor: '#000066', borderWidth: 1 / PixelRatio.get(),
                                    borderRadius: 5,// margin: 5,
                                    backgroundColor: '#3496f0'
                                }}
                                >

                                    <Text
                                        style={{
                                            borderColor: '#9B9B9B',
                                            borderWidth: 0 / PixelRatio.get(),
                                            margin: 5,
                                            padding: 5,
                                            fontSize: 11,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                        }}>Choose ESP file...</Text>
                                </TouchableOpacity>
                                <Text style={{
                                    borderColor: '#9B9B9B',
                                    borderWidth: 0 / PixelRatio.get(),
                                    margin: 5,
                                    padding: 10,
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                    textAlign: 'center',

                                }}>{this.state.espName}</Text>
                                {this.state.imagefile ? <Button
                                    buttonStyle={{
                                        backgroundColor: '#9B9B9B',
                                        borderColor: 'transparent',
                                        borderWidth: 5,
                                        borderRadius: 15,
                                        width: 200,
                                        // marginTop: 50,
                                    }}
                                    title="ESP Update"
                                    onPress={() => {
                                        sendFile(BleManager, {
                                                peripheralId: this.state.selectedItem.itemID,
                                                serviceId: "6f400051-b5a3-f393-e0a9-e50e24dcca9e",
                                                characteristic: "6f400002-b5a3-f393-e0a9-e50e24dcca9e",
                                            }, this.state.imagefile,
                                            (progress) => {
                                                this.setState({progress: progress})
                                            }).catch(err => {
                                            if (err) {
                                                this.error()
                                            }
                                        });
                                    }}/> : null}
                                <View style={{flexDirection: 'row', marginTop: 10}}>
                                    <View
                                        style={{backgroundColor: '#9B9B9B', height: 2, flex: 1, alignSelf: 'center'}}/>
                                    <View
                                        style={{backgroundColor: '#9B9B9B', height: 2, flex: 1, alignSelf: 'center'}}/>
                                </View>
                                <Text style={{
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    textAlign: 'center',
                                    marginTop: 20
                                }}> Profile Transfer </Text>
                                <Button
                                    buttonStyle={{
                                        backgroundColor: '#9B9B9B',
                                        borderColor: 'transparent',
                                        borderWidth: 5,
                                        borderRadius: 15,
                                        width: 200,
                                        marginTop:10,
                                    }}
                                    title=" Profile Update"
                                    onPress={() => {
                                        const jsonSample = profilej;
                                        psendFile(BleManager, {
                                                peripheralId: this.state.selectedItem.itemID,
                                                serviceId: "6e400001-b5a3-f393-e0a9-e50e24dcca9e",
                                                characteristic: "6e400002-b5a3-f393-e0a9-e50e24dcca9e",
                                            }, jsonSample,
                                            (progress) => {
                                                this.setState({progress: progress})

                                            }).catch(err => {
                                            if (err) {
                                                this.error()
                                            }
                                        })
                                    }}/>
                                {this.state.progress > 0 ? <Progress.Circle
                                    style={{marginTop: 20}}

                                    fillStyle={{color: 'black'}}
                                    size={100}
                                    showsText={true}
                                    backgroundStyle={{
                                        backgroundColor: '#cccccc',
                                        borderRadius: 2,
                                        color: '#8d9399',

                                    }}

                                    progress={this.state.progress}
                                /> : null}

                            </View>
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'image'}
                            title="DFU Update"
                            selectedTitleStyle={{color: "#3496f0"}}
                            renderIcon={() => <Icon name="cog" size={px2dp(22)} color="#666"/>}
                            renderSelectedIcon={() => <Icon name="cog" size={px2dp(22)}
                                                            color="#3496f0"/>}
                            onPress={() => {
                                this.setState({selectedTab: 'image'});
                                this.handledfu(this.state.selectedItem.itemID);
                            }}>
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                backgroundColor: "#F5FCFF",

                            }}>


                                    <Text style={{
                                        fontSize: 25,
                                        padding: 15,
                                        fontWeight: 'bold',
                                        textAlign: 'center',
                                    }}> DFU Update </Text>
                                    <Text style={{fontWeight: 'bold',marginTop:30}}>
                                        (Device is in bootloader mode)
                                    </Text>
                                    <Text style={styles.welcome}>
                                        {this.state.dfuState}
                                    </Text>
                                    <Text style={styles.welcome}>
                                        {"DFU progress: " + this.state.uprogress + " %"}
                                    </Text>
                                    <Text style={{fontWeight: 'bold'}}>
                                        {"Found device: " + this.state.selectedItem.itemID}
                                    </Text>
                                    <Text/>
                                    {this.state.file ? <TouchableHighlight
                                        underlayColor="0A64C2" style={{
                                        borderColor: '#000066', borderWidth: 1 / PixelRatio.get(),
                                        borderRadius: 5, marginTop: 20,
                                        backgroundColor: '#9B9B9B'
                                    }}
                                        onPress={() => {

                                            this.startDFU(5)

                                        }}>
                                        <Text style={{
                                            borderColor: '#9B9B9B',
                                            borderWidth: 0 / PixelRatio.get(),
                                            margin: 5,
                                            padding: 5,
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            color: '#333333',
                                        }}>Start DFU</Text>
                                    </TouchableHighlight> : null}


                                    <TouchableOpacity
                                        underlayColor="0A64C2" style={{
                                        borderColor: '#000066', borderWidth: 1 / PixelRatio.get(),
                                        borderRadius: 5, marginTop: 40,
                                        backgroundColor: '#3496f0'
                                    }}
                                        onPress={this.selectFileTapped.bind(this)}>
                                        <Text style={{
                                            borderColor: '#9B9B9B',
                                            borderWidth: 0 / PixelRatio.get(),
                                            margin: 5,
                                            padding: 5,
                                            fontSize: 10,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            color: '#333333',
                                        }}>Choose file...</Text>
                                    </TouchableOpacity>
                                    <Text style={{
                                        borderColor: '#9B9B9B',
                                        borderWidth: 0 / PixelRatio.get(),
                                        margin: 5,
                                        padding: 5,
                                        fontSize: 10,
                                        fontWeight: 'bold',
                                        textAlign: 'center',
                                        color: '#333333',
                                    }}>{this.state.nameFile}</Text>

                            </View>
                        </TabNavigator.Item>
                    </TabNavigator>
                    : <View style={{
                        flex: 1,
                        alignItems: 'center',
                        backgroundColor: 'white',
                        marginTop: 50
                    }}>
                        <View style={{
                            flex: 1, justifyContent: 'center',
                            alignItems: 'center',
                        }}>


                        </View>
                    </View>}
                </Modal>


                <ScrollView style={styles.scroll}>
                    {(list.length === 0) &&
                    <View style={{flex: 1, margin: 20, alignItems: 'center', justifyContent: 'center', marginTop: 120}}>
                        <Icon name="bluetooth-b" size={70}/>
                        <Text style={{textAlign: 'center', marginTop: 10}}>No devices</Text>
                    </View>
                    }
                    <ListView
                        enableEmptySections={true}
                        dataSource={dataSource}
                        renderRow={(item) => {
                            let battery;

                            //for Android
                            function advertisingPackageConvertor(bytes) {
                                let result = {};
                                for (let i = 0; i < bytes.length;) {
                                    let size = bytes[i] - 1;
                                    let t = bytes[i + 1];
                                    let data = bytes.slice(i + 2, i + 2 + size);
                                    t = typeMapper[t] || t;
                                    result[t] = data.map(c => String.fromCharCode(c)).join('');
                                    i += (size + 2);
                                }
                                return result;
                            }

                            let bytes;
                            if (Platform.OS === 'android') {
                                let obj = advertisingPackageConvertor(item.advertising.bytes);
                                bytes = obj[22].split('').map(m => m.charCodeAt()).slice(2);

                            } else {
                                bytes = item.advertising.kCBAdvDataServiceData.AAAA.bytes
                            }

                            let batt = bytes[0];
                            if (batt) {
                                battery = batt + '%'
                            } else {
                                battery = 0 + '%'
                            }

                            let itemID = item.id;
                            let name;
                            if (Platform.OS === 'android') {
                                name = item.name
                            } else {
                                name = item.advertising.kCBAdvDataLocalName;
                            }
                            const color = item.connected ? '#4F8EF7' : '#fff';


                            function toInt(a) {
                                let start = 0;
                                for (let bit of a) {
                                    start = start * 256 + bit;
                                }
                                return start;
                            }

                            let imeiARR = []
                            imeiARR.push(bytes[10], bytes[11], bytes[12], bytes[13], bytes[14], bytes[15], bytes[16], bytes[17])
                            let chepARR = [];
                            chepARR.push(bytes[1], bytes[2], bytes[3], bytes[4], bytes[5]);
                            let chep = toInt(chepARR)
                            let ver1 = bytes[6];
                            let ver2 = bytes[7]
                            let ver3 = bytes[8];
                            let type = bytes[9];


                            let buffer = Buffer.from(imeiARR);
                            let imeib = new Uint64BE(buffer);
                            let imei = imeib.toString();


                            const itemData = {
                                name,
                                chep,
                                ver1,
                                ver2,
                                ver3,
                                type,
                                imei,
                                itemID
                            };

                            return (
                                <TouchableHighlight onPress={() => {
                                    this.setState({selectedItem: itemData,});
                                    if (item.connected) {
                                        this.setState({spinner: false});
                                    } else {
                                        this.setState({spinner: !this.state.spinner});
                                        setTimeout(() => {

                                            if (this.state.spinner = !this.state.spinner) {
                                            }
                                            this.setState({spinner: false})

                                        }, 5000)
                                    }
                                    this.test(item)
                                }} underlayColor="#000066" style={{
                                    borderColor: '#000066', borderWidth: 1.5,
                                    borderRadius: 10, margin: 4
                                }}>
                                    <View style={[styles.row, {backgroundColor: color}]}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            color: '#333333',
                                            padding: 7
                                        }}> {name}</Text>

                                        <Text style={{
                                            fontSize: 10,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            color: '#333333',
                                            padding: 5
                                        }}>ID:{item.id}</Text>
                                        <Text style={{
                                            fontSize: 10,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            color: '#333333',
                                            padding: 5
                                        }}><Icon name="rss" size={12}/> RSSI:{item.rssi} dBm</Text>
                                        <Text style={{
                                            fontSize: 10,
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                            color: '#333333',
                                            padding: 5
                                        }}><Icon name="battery" size={10} style={{marginTop: 50}}/> Battery:{battery}
                                        </Text>
                                    </View>
                                </TouchableHighlight>
                            );
                        }}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        width: window.width,
        height: window.height
    },
    container2: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        width: window.width,
        height: window.height
    },

    scroll: {
        flex: 1,
        backgroundColor: '#f0f0f0',
        margin: 10,
    },
    row: {

        padding: 10,
        borderRadius: 10
    },
    nav: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'white'


    },
    welcome: {
        fontWeight: 'bold',

        fontSize: 20,
        textAlign: "center",
        margin: 10
    },
    instructions: {
        fontWeight: 'bold',
        textAlign: "center",
        color: "#333333",
        marginBottom: 5
    }
});


