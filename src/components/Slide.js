import React from 'react';
import { StyleSheet } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Actions} from 'react-native-router-flux';

const styles = StyleSheet.create({
    image: {
        width: 320,
        height: 320,
    },
    text: {
        color: 'red',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 16,
    }

});

const slides = [
    {
        key: 'somethun',
        title: '',
        text: '',
        image: require('../images/bxb.png'),
        imageStyle: styles.image,
        backgroundColor: '#4F8EF7',
    },

    {
        key: 'somethun1',
        title: 'Instructions',
        text: 'Turn on your mobile device Bluetooth.Press Scan button to scan the devices.Connect to the device by clicking  on a list item.',



        image: require('../images/Config3.png'),
        imageStyle: styles.image,
        backgroundColor: '#4F8EF7',
    }
];

export default class App extends React.Component {



    _onDone = () => {
        Actions.Scan()
    }
    render() {
        return (
            <AppIntroSlider
                slides={slides}
                onDone={()=> {Actions.scan()}}
            />
        );
    }
}