import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';
import Slide from './components/Slide'
import Scan from './components/Scan';


export default class Routes extends Component<{}> {
    render() {
        return(
            <Router>
                <Stack key="root" hideNavBar={true}>

                    <Scene key="indexpage" component={Slide} title="Slide" initial={true}/>
                    <Scene key= 'scan'component={Scan} title="Scan" />



                </Stack>
            </Router>
        )
    }
}