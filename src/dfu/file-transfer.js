import {Buffer} from 'buffer';

let crcTable = null;

function makeCRCTable() {
    let c;
    const crcTable = [];
    for (let n = 0; n < 256; n++) {
        c = n;
        for (let k = 0; k < 8; k++) {
            c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
    }
    return crcTable;
}

function crc32(data) {
    if (!crcTable) {
        crcTable = makeCRCTable();
    }

    let crc = 0 ^ (-1);

    for (let i = 0; i < data.length; i++) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ data[i]) & 0xFF];
    }

    return (crc ^ (-1)) >>> 0;
}

function toBytesInt32(num) {
    return [
        (num & 0xff000000) >> 24,
        (num & 0x00ff0000) >> 16,
        (num & 0x0000ff00) >> 8,
        (num & 0x000000ff)
    ]
}


function toBytesInt16(num) {
    return [
        (num & 0x0000ff00) >> 8,
        (num & 0x000000ff)
    ]
}

async function sendFile(manager, {peripheralId, serviceId, characteristic}, json, progressHandler) {
    progressHandler = progressHandler || (p => console.log(p));

    let buf =Buffer.from(json, 'base64')

    const data =  [...buf]

    // data to 18 byte chunks, remaining 2 bytes for chunk number
    const overallChunks = Math.ceil(data.length / 240);
    let chunkNumber = 0;
    for (let chunk of fileChunk(data)) {
        console.log(`chunk ${chunkNumber}: `, chunk);
        let tries = 5;
        while (tries > 0) {
            try {

                await manager.write(peripheralId, serviceId, characteristic, chunk,247);
                chunkNumber++;
                progressHandler(chunkNumber / overallChunks);
                break;
            } catch (err) {
                tries--;
                await delay(100)
            }
        }
        if (tries === 0) {
            throw new Error("Max tries count exceeded")
        }
    }
}

function* fileChunk(data) {
    let i = 0;
    let chunkNumber = 0;
    while (i <= data.length) {
        yield toBytesInt16(chunkNumber).concat(data.slice(i, i + 238));
        i += 238;
        chunkNumber++;
    }

    // send checksum and final signal, 4 byte all filled by 1, and then checksum in 4 bytes to
    const checksum = crc32(data);
    let lastChunk = new Array(2).fill(255); // 4 bytes of 1 111111....111
    lastChunk = lastChunk.concat(toBytesInt32(checksum)); // fill with checksum and 0's
    yield lastChunk;
}

const delay = ms => {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
};
export default sendFile;
